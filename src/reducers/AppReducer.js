import immutable from 'seamless-immutable';
import { Type as AuthActions } from '../actions/AuthActions';
import uuid from 'uuid-random';

export const INITIAL_STATE = immutable({
    token: null,
    logInFailed: false,
    checkingCredentials: false,
    loginSuccess: false,
    personSuccesfullyAdded: false,
    user: {},
    genders: [
        { label: 'Male', value: 'male', id: 1 },
        { label: 'Female', value: 'female', id: 2 },
    ],
    persons: [],
    person: {
        id: null,
        age: '',
        name: '',
        gender: ''
    }
});

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {

        case AuthActions.SET_INITIAL_STATE: {
            let componentToSetInitialState = {};
            let value = INITIAL_STATE[action.data];
            componentToSetInitialState[action.data] = value;
            return state.merge({ ...componentToSetInitialState });
            break;
        }

        case AuthActions.LOGOUT:
            localStorage.clear();
            window.location.replace(window.location.origin);
            return INITIAL_STATE;
            break;

        case AuthActions.LOGIN_REST_CALL:
            let checkingCredentials = true;
            return state.merge({ checkingCredentials });
            break;

        case AuthActions.LOGIN_SUCCESS: {
            let token = action.data.token;
            let user = action.data.korisnik;
            let checkingCredentials = false;
            console.log('user', user);
            console.log('token', token);
            localStorage.setItem('token', token);
            localStorage.setItem('user', JSON.stringify(user));
            let loginSuccess = true;
            return state.merge({ token, loginSuccess, user, checkingCredentials });
            break;
        }

        case AuthActions.SET_VALUE: {
            let property = action.value;
            let person = state.person.asMutable();
            person[action.name] = property;
            return state.merge({ person });
            break;
        }

        case AuthActions.LOGIN_FAILED:
            let logInFailed = true;
            return state.merge({ logInFailed });
            break;

        case AuthActions.ADD_PERSON: {
            let person = state.person.asMutable();
            let persons = state.persons.asMutable();
            person.id = uuid();
            persons.push(person);
            return state.merge({ persons, personSuccesfullyAdded: true })
        }


        default:
            return state;
    }
}
