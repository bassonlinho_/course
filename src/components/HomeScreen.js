import React from 'react';
import { connect } from 'react-redux';
import { logout, addPerson, setInitialState, setValueInReducer } from '../actions/AuthActions'
import { Button, TextField } from '@material-ui/core';
export class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.personSuccesfullyAdded === false && this.props.personSuccesfullyAdded === true) {
            this.props.setInitialState('personSuccesfullyAdded')
            this.props.setInitialState('person')
        }
    }

    add() {
        if (this.props.person.age && this.props.person.name && this.props.person.gender) {
            this.props.addPerson(this.props.person);
        } else {
            alert('Required fields must be entered!');
        }
    }

    handleChangeReducer = (field, value) => {
        this.props.setValueInReducer(field, value);
    }

    render() {
        let mapedGenders = this.props.genders.map((gender) => {
            return <option key={gender.id} id={gender.id} value={gender.value}>{gender.label}</option>
        })
        let list;
        if (this.props.persons.length !== 0) {
            list = this.props.persons.map((person) => {
                return <div key={person.age}>{person.id + ' | ' + person.age + ' | ' + person.name + ' | ' + person.gender}</div>
            })
        }
        console.log('persons', this.props.persons.asMutable());
        return (
            <div>
                <Button
                    onClick={() => this.props.logout()}>
                    {'Logout'}
                </Button>

                <div style={{ display: 'flex', flexDirection: 'column', margin: '50px' }}>
                    <TextField
                        required
                        label='Age'
                        value={this.props.person.age}
                        type='number'
                        style={{ width: '150px' }}
                        onChange={(event) => this.handleChangeReducer('age', event.target.value)}
                    />
                    <TextField
                        required
                        value={this.props.person.name}
                        label="Name"
                        style={{ width: '150px' }}
                        onChange={(event) => this.handleChangeReducer('name', event.target.value)}
                    />
                    <TextField
                        required
                        label='Gender'
                        SelectProps={{
                            native: true,
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        style={{ width: '150px' }}
                        value={this.props.person.gender}
                        margin="normal"
                        onChange={(event) => this.handleChangeReducer('gender', event.target.value)}
                        select>
                        <option value={null}>Select...</option>
                        {mapedGenders}
                    </TextField>
                    <Button style={{ width: '150px' }}
                        onClick={() => this.add()}>
                        Add
                    </Button>
                </div>
                {list}
            </div>
        )
    }
}




function mapStateToProps(state) {
    return {
        genders: state.appReducer.genders,
        persons: state.appReducer.persons,
        person: state.appReducer.person,
        personSuccesfullyAdded: state.appReducer.personSuccesfullyAdded
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logout: () => dispatch(logout()),
        addPerson: () => dispatch(addPerson()),
        setValueInReducer: (name, value) => dispatch(setValueInReducer(name, value)),
        setInitialState: (component) => dispatch(setInitialState(component))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeScreen)